from argparse import ArgumentParser
from imgurpython import ImgurClient
from imgurpython.helpers.error import ImgurClientError
from os import path
import sys

# Requires imgurpython
# pip3 install imgurpython
# Obtain API keys by registering on imgur.com
# Go into settings, applications, and copy keys here

CLIENT_ID = ''
CLIENT_SECRET = ''

MTL_TEMPLATE = """\
newmtl image
Ns 96.078431
Ka 1.000000 1.000000 1.000000
Kd 0.640000 0.640000 0.640000
Ks 0.500000 0.500000 0.500000
Ke 0.000000 0.000000 0.000000
Ni 1.000000
d 1.000000
illum 2
map_Kd {0}
"""
OBJ_TEMPLATE = """\
mtllib {0}
o Guy.001
v -0.913707 0.127244 0.058789
v -0.888529 1.890936 0.014599
v 0.941167 1.890936 0.014599
v 0.915989 0.127244 0.058789
vt 0.000000 0.000000
vt 0.000000 1.000000
vt 1.000000 1.000000
vt 1.000000 0.000000
vn 0.000000 -0.025000 -0.999700
usemtl image
s 1
f 1/1/1 2/2/1 3/3/1 4/4/1
"""

def fetch_album_images(album_id):
    c = ImgurClient(CLIENT_ID, CLIENT_SECRET)
    return c.get_album_images(album_id)

def make_parser():
    parser = ArgumentParser()
    parser.add_argument('album')
    parser.add_argument('--directory', default='.')
    return parser

def write_mtl_obj(image, directory='.'):
    fname = image.id + '.mtl'
    with open(path.join(directory, fname), 'wt', newline='\n') as f:
        f.write(MTL_TEMPLATE.format(image.link))
    with open(path.join(directory, image.id + '.obj'), 'wt', newline='\n') as f:
        f.write(OBJ_TEMPLATE.format(fname))

def main():
    parser = make_parser()
    args = parser.parse_args()
    try:
        images = fetch_album_images(args.album)
    except ImgurClientError as e:
        print(e)
        sys.exit(1)
    for image in images:
        write_mtl_obj(image, directory=args.directory)
        print(image.link)

if __name__ == '__main__':
    main()
