#!/bin/bash

hash=`ipfs add -wq *.obj *.mtl | tail -n1`

for filename in $(ls *.obj)
do
cat << EOF > ${filename%.*}.txt
<FireBoxRoom>
<Assets>
<AssetObject id="body" mtl="http://ipfs.io/ipfs/$hash/${filename%.*}.mtl" src="http://ipfs.io/ipfs/$hash/${filename%.*}.obj" />
<AssetObject id="head" mtl="" src="" />
</Assets>
<Room>
<Ghost id="kool" scale="1 1 1" lighting="false" body_id="body" anim_id="type" userid_pos="0 0.5 0" cull_face="none" />
</Room>
</FireBoxRoom>
EOF
done
