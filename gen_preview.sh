#!/bin/bash
# Reads from images.txt file a list of absolute imgur links and creates an HTML for avatar selection
hash=`ipfs add -wq *.obj *.mtl *.txt | tail -n1`

echo "<html>"
echo "<body>"
echo "<ul>"

while IFS='' read -r line || [ -n "$line" ]; do
    base="${line##*/}"
    echo "<li style='display:inline-block'><a href='https://ipfs.io/ipfs/$hash/${base%.*}.txt'><img src='$line'></a>"
done < images.txt

echo "</ul>"
echo "</body>"
echo "</html>"
