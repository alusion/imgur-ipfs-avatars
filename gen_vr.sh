#!/bin/bash

tree=`ipfs add -wq *.obj *.mtl | tail -n1`

for filename in $(ls *.obj)
do
cat << EOF > ${filename%.*}.html
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Memes">
    <meta name="author" content="">
</head>
<body>
<!--
<FireBoxRoom>
<Assets>
<AssetObject id="${filename##.*}" src="http://ipfs.io/ipfs/$tree/${filename%.*}.obj" mtl="http://ipfs.io/ipfs/$tree/${filename%.*}.mtl" />
</Assets>
<Room>
<Object id="${filename##.*}" js_id="-0-1476656711" lighting="true" pos="0 0 4" xdir="1 0.000001 0.000001" ydir="-0.000001 1 -0.000002" zdir="-0.000001 0.000002 1" cull_face="none" />
</Room>
</FireBoxRoom>
-->
<script src="https://web-staging.janusvr.com/janusweb.js"></script>
<script>elation.janusweb.init({url: document.location.href})</script>
</body>
</html>
EOF
done
